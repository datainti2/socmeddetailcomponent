import { Component } from '@angular/core';

@Component({
  selector: 'sample-component',
  template: `<h1>Sample component</h1><app-base-widget></app-base-widget>`
})
export class SampleComponent {

  constructor() {
  }

}
