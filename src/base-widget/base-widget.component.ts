import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {

  public dataLoader: Array<any> = [{"name": "Kaka","image": "../assets/twitter.png","count": "234","web": "http://google.com",	"link": "track",	"friend": "234.234",	"location": "parigi baru"}];

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}
 